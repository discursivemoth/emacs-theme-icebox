;;; icebox-light-hard-theme.el --- A retro-groove colour theme for Emacs

;; Copyright (c) 2013 Lee Machin
;; Copyright (c) 2013-2016 Eduardo Lavaque
;; Copyright (c) 2016-2017 Jason Milkins
;; Copyright (c) 2017-2018 Martijn Terpstra

;; Author: Jason Milkins <jasonm23@gmail.com>
;; (current maintainer)
;;
;; Author-list: Lee Machin <ljmachin@gmail.com>,
;;              Eduardo Lavaque <me@greduan.com>
;;
;; URL: http://github.com/greduan/emacs-theme-icebox
;; Version: 1.26.0

;; Package-Requires: ((autothemer "0.2"))

;;; Commentary:

;; Using autothemer since 1.00

;; A port of the Gruvbox colorscheme for Vim, built on top of the new built-in
;; theme support in Emacs 24.
;;
;; This theme contains my own modifications and it's a bit opinionated
;; sometimes, deviating from the original because of it. I try to stay
;; true to the original as much as possible, however. I only make
;; changes where I would have made the changes on the original.
;;
;; Since there is no direct equivalent in syntax highlighting from Vim to Emacs
;; some stuff may look different, especially in stuff like JS2-mode, where it
;; adds stuff that Vim doesn't have, in terms of syntax.

;;; Credits:

;; Pavel Pertsev created the original theme for Vim, on which this port
;; is based.

;; Lee Machin created the first port of the original theme, which
;; Greduan developed further adding support for several major modes.
;;
;; Jason Milkins (ocodo) has maintained the theme since 2015 and is
;; working with the community to add further mode support and align
;; the project more closely with Vim Gruvbox.

;;; Code:
(eval-when-compile
  (require 'cl-lib))

(require 'icebox)

(icebox-deftheme
 icebox-light-hard
 "A retro-groove colour theme (light version, hard contrast)"

 ((((class color) (min-colors #xFFFFFF))        ; col 1 GUI/24bit
   ((class color) (min-colors #xFF)))           ; col 2 Xterm/256

  (icebox-dark0_hard      "#f9f5d7" "#ffffd7")
  (icebox-dark0           "#fbf1c7" "#ffffd7")
  (icebox-dark0_soft      "#f2e5bc" "#ffffd7")
  (icebox-dark1           "#ebdbb2" "#ffffaf")
  (icebox-dark2           "#d5c4a1" "#d7d6af")
  (icebox-dark3           "#bdae93" "#afaf87")
  (icebox-dark4           "#a89984" "#afafaf")

  (icebox-gray            "#928374" "#8a8a8a")

  (icebox-light0_hard     "#1d2021" "#1c1c1c")
  (icebox-light0          "#282828" "#262626")
  (icebox-light0_soft     "#32302f" "#303030")
  (icebox-light1          "#3c3836" "#3a3a3a")
  (icebox-light2          "#504945" "#4e4e4e")
  (icebox-light3          "#665c54" "#626262")
  (icebox-light4          "#7c6f64" "#767676")

  (icebox-bright_red      "#9d0006" "#870000")
  (icebox-bright_green    "#79740e" "#878700")
  (icebox-bright_yellow   "#b57614" "#af8700")
  (icebox-bright_blue     "#076678" "#005f87")
  (icebox-bright_purple   "#8f3f71" "#875f87")
  (icebox-bright_aqua     "#427b58" "#5f8787")
  (icebox-bright_orange   "#af3a03" "#af5f00")

  (icebox-faded_red       "#cc241d" "#d75f5f")
  (icebox-faded_green     "#98971a" "#afaf00")
  (icebox-faded_yellow    "#d79921" "#ffaf00")
  (icebox-faded_blue      "#458588" "#87afaf")
  (icebox-faded_purple    "#b16286" "#d787af")
  (icebox-faded_aqua      "#689d6a" "#87af87")
  (icebox-faded_orange    "#d65d0e" "#ff8700")

  (icebox-dark_red        "#421E1E" "#5f0000")
  (icebox-dark_blue       "#2B3C44" "#000087")
  (icebox-dark_aqua       "#36473A" "#005f5f")

  (icebox-delimiter-one   "#458588" "#008787")
  (icebox-delimiter-two   "#b16286" "#d75f87")
  (icebox-delimiter-three "#8ec07c" "#87af87")
  (icebox-delimiter-four  "#d65d0e" "#d75f00")
  (icebox-white           "#FFFFFF" "#FFFFFF")
  (icebox-black           "#000000" "#000000")
  (icebox-sienna          "#DD6F48" "#d7875f")
  (icebox-darkslategray4  "#528B8B" "#5f8787")
  (icebox-lightblue4      "#66999D" "#5fafaf")
  (icebox-burlywood4      "#BBAA97" "#afaf87")
  (icebox-aquamarine4     "#83A598" "#87af87")
  (icebox-turquoise4      "#61ACBB" "#5fafaf")

  (icebox-bg icebox-dark0_hard))

 (custom-theme-set-variables 'icebox-light-hard
                             `(ansi-color-names-vector
                               [,icebox-dark1
                                ,icebox-faded_red
                                ,icebox-faded_green
                                ,icebox-faded_yellow
                                ,icebox-faded_blue
                                ,icebox-faded_purple
                                ,icebox-faded_aqua
                                ,icebox-light1])))

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'icebox-light-hard)

;; Local Variables:
;; eval: (when (fboundp 'rainbow-mode) (rainbow-mode +1))
;; End:

;;; icebox-light-hard-theme.el ends here
