;;; icebox-dark-theme.el --- A retro-groove colour theme for Emacs

;; Copyright (c) 2013 Lee Machin
;; Copyright (c) 2013-2016 Eduardo Lavaque
;; Copyright (c) 2016-2017 Jason Milkins
;; Copyright (c) 2017 Martijn Terpstra

;; Author: Jason Milkins <jasonm23@gmail.com>
;; (current maintainer)
;;
;; Author-list: Lee Machin <ljmachin@gmail.com>,
;;              Eduardo Lavaque <me@greduan.com>
;;
;; URL: http://github.com/greduan/emacs-theme-icebox
;; Version: 1.26.0

;; Package-Requires: ((autothemer "0.2"))

;;; Commentary:

;; Using autothemer since 1.00

;; A port of the Gruvbox colorscheme for Vim, built on top of the new built-in
;; theme support in Emacs 24.
;;
;; This theme contains my own modifications and it's a bit opinionated
;; sometimes, deviating from the original because of it. I try to stay
;; true to the original as much as possible, however. I only make
;; changes where I would have made the changes on the original.
;;
;; Since there is no direct equivalent in syntax highlighting from Vim to Emacs
;; some stuff may look different, especially in stuff like JS2-mode, where it
;; adds stuff that Vim doesn't have, in terms of syntax.

;;; Credits:

;; Pavel Pertsev created the original theme for Vim, on which this port
;; is based.

;; Lee Machin created the first port of the original theme, which
;; Greduan developed further adding support for several major modes.
;;
;; Jason Milkins (ocodo) has maintained the theme since 2015 and is
;; working with the community to add further mode support and align
;; the project more closely with Vim Gruvbox.
;;
;; Martijn Terpstra has been a major contributor since mid 2017 and
;; helped to re-implement Gruvbox with autothemer so we can have
;; multiple variants of Gruvbox (as we do on Vim).  Martijn has also
;; provided a large number mode support enhancements.

;;; Code:
(eval-when-compile
  (require 'cl-lib))

(require 'autothemer)

(unless (>= emacs-major-version 24)
  (error "Requires Emacs 24 or later"))
;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(defvar icebox-screenshot-command "scrot -u %s%s.png"
  "Command used to take automated screenshots for icebox.
Should contain 2 %s constructs to allow for theme name and directory/prefix")

(defun icebox-screenshot (prefix)
  "Take a screenshot of all versions of the icebox theme"
  (interactive "sScreenshot Prefix: ")
  (dolist (theme '(icebox-light-soft
                   icebox-light-medium
                   icebox-light-hard
                   icebox-dark-soft
                   icebox-dark-medium
                   icebox-dark-hard))
    (load-theme theme t)
    (redisplay t)
    (shell-command (format icebox-screenshot-command
                           prefix theme))))

(defmacro icebox-deftheme (name description palette reduced-specs &rest body)
  `(autothemer-deftheme
    ,name
    ,description
    ,palette
    ((default                                   (:background icebox-bg :foreground icebox-light0))
     (cursor                                    (:background icebox-light0))
     (mode-line                                 (:background icebox-dark3 :foreground icebox-light2 :box nil))
     (mode-line-inactive                        (:background icebox-dark1 :foreground icebox-light4 :box nil))
     (fringe                                    (:background icebox-bg))
     (hl-line                                   (:background icebox-dark1))
     (region                                    (:background icebox-dark2)) ;;selection
     (secondary-selection                       (:background icebox-dark1))
     (minibuffer-prompt                         (:background icebox-bg :foreground icebox-bright_green :bold t))
     (vertical-border                           (:foreground icebox-dark2))
     (window-divider                            (:foreground icebox-dark2))
     (link                                      (:foreground icebox-faded_blue :underline t))
     (shadow                                    (:foreground icebox-dark4))

     ;; Built-in syntax

     (font-lock-builtin-face                            (:foreground icebox-bright_orange))
     (font-lock-constant-face                           (:foreground icebox-bright_purple))
     (font-lock-comment-face                            (:foreground icebox-dark4))
     (font-lock-function-name-face                      (:foreground icebox-bright_yellow))
     (font-lock-keyword-face                            (:foreground icebox-bright_red))
     (font-lock-string-face                             (:foreground icebox-bright_green))
     (font-lock-variable-name-face                      (:foreground icebox-bright_blue))
     (font-lock-type-face                               (:foreground icebox-bright_purple))
     (font-lock-warning-face                            (:foreground icebox-bright_red :bold t))

     ;; Basic faces
     (error                                             (:foreground icebox-bright_red :bold t))
     (success                                           (:foreground icebox-bright_green :bold t))
     (warning                                           (:foreground icebox-bright_yellow :bold t))
     (trailing-whitespace                               (:background icebox-bright_red))
     (escape-glyph                                      (:foreground icebox-bright_aqua))
     (header-line                                       (:background icebox-dark0 :foreground icebox-light3 :box nil :inherit nil))
     (highlight                                         (:background icebox-dark4 :foreground icebox-light0))
     (homoglyph                                         (:foreground icebox-bright_yellow))
     (match                                             (:foreground icebox-dark0 :background icebox-bright_blue))

     ;; Customize faces
     (widget-field                                      (:background icebox-dark3))
     (custom-group-tag                                  (:foreground icebox-bright_blue :weight 'bold))
     (custom-variable-tag                               (:foreground icebox-bright_blue :weight 'bold))

     ;; whitespace-mode

     (whitespace-space                          (:background icebox-bg :foreground icebox-dark4))
     (whitespace-hspace                         (:background icebox-bg :foreground icebox-dark4))
     (whitespace-tab                            (:background icebox-bg :foreground icebox-dark4))
     (whitespace-newline                        (:background icebox-bg :foreground icebox-dark4))
     (whitespace-trailing                       (:background icebox-dark1 :foreground icebox-bright_red))
     (whitespace-line                           (:background icebox-dark1 :foreground icebox-bright_red))
     (whitespace-space-before-tab               (:background icebox-bg :foreground icebox-dark4))
     (whitespace-indentation                    (:background icebox-bg :foreground icebox-dark4))
     (whitespace-empty                          (:background nil :foreground nil))
     (whitespace-space-after-tab                (:background icebox-bg :foreground icebox-dark4))

     ;; RainbowDelimiters

     (rainbow-delimiters-depth-1-face           (:foreground icebox-delimiter-one))
     (rainbow-delimiters-depth-2-face           (:foreground icebox-delimiter-two))
     (rainbow-delimiters-depth-3-face           (:foreground icebox-delimiter-three))
     (rainbow-delimiters-depth-4-face           (:foreground icebox-delimiter-four))
     (rainbow-delimiters-depth-5-face           (:foreground icebox-delimiter-one))
     (rainbow-delimiters-depth-6-face           (:foreground icebox-delimiter-two))
     (rainbow-delimiters-depth-7-face           (:foreground icebox-delimiter-three))
     (rainbow-delimiters-depth-8-face           (:foreground icebox-delimiter-four))
     (rainbow-delimiters-depth-9-face           (:foreground icebox-delimiter-one))
     (rainbow-delimiters-depth-10-face          (:foreground icebox-delimiter-two))
     (rainbow-delimiters-depth-11-face          (:foreground icebox-delimiter-three))
     (rainbow-delimiters-depth-12-face          (:foreground icebox-delimiter-four))
     (rainbow-delimiters-unmatched-face         (:background nil :foreground icebox-light0))


     ;; line numbers
     (line-number                               (:foreground icebox-dark4 :background icebox-dark1))
     (line-number-current-line                  (:foreground icebox-bright_orange :background icebox-dark2))
     (linum                                     (:foreground icebox-dark4 :background icebox-dark1))
     (linum-highlight-face                      (:foreground icebox-bright_orange :background icebox-dark2))
     (linum-relative-current-face               (:foreground icebox-bright_orange :background icebox-dark2))

     ;; Highlight indentation mode
     (highlight-indentation-current-column-face (:background icebox-dark2))
     (highlight-indentation-face                (:background icebox-dark1))

     ;; smartparens
     (sp-pair-overlay-face                      (:background icebox-dark2))
     (sp-show-pair-match-face                   (:background icebox-dark2)) ;; Pair tags highlight
     (sp-show-pair-mismatch-face                (:background icebox-bright_red)) ;; Highlight for bracket without pair
     ;;(sp-wrap-overlay-face                     (:inherit 'sp-wrap-overlay-face))
     ;;(sp-wrap-tag-overlay-face                 (:inherit 'sp-wrap-overlay-face))

     ;; elscreen
     (elscreen-tab-background-face              (:background icebox-bg :box nil)) ;; Tab bar, not the tabs
     (elscreen-tab-control-face                 (:background icebox-dark2 :foreground icebox-bright_red :underline nil :box nil)) ;; The controls
     (elscreen-tab-current-screen-face          (:background icebox-dark4 :foreground icebox-dark0 :box nil)) ;; Current tab
     (elscreen-tab-other-screen-face            (:background icebox-dark2 :foreground icebox-light4 :underline nil :box nil)) ;; Inactive tab

     ;; ag (The Silver Searcher)
     (ag-hit-face                               (:foreground icebox-bright_blue))
     (ag-match-face                             (:foreground icebox-bright_red))

     ;; Diffs
     (diff-changed                              (:background nil :foreground icebox-light1))
     (diff-added                                (:background nil :foreground icebox-bright_green))
     (diff-removed                              (:background nil :foreground icebox-bright_red))
     (diff-indicator-changed                    (:inherit 'diff-changed))
     (diff-indicator-added                      (:inherit 'diff-added))
     (diff-indicator-removed                    (:inherit 'diff-removed))

     (js2-warning                               (:underline (:color icebox-bright_yellow :style 'wave)))
     (js2-error                                 (:underline (:color icebox-bright_red :style 'wave)))
     (js2-external-variable                     (:underline (:color icebox-bright_aqua :style 'wave)))
     (js2-jsdoc-tag                             (:background nil :foreground icebox-gray  ))
     (js2-jsdoc-type                            (:background nil :foreground icebox-light4))
     (js2-jsdoc-value                           (:background nil :foreground icebox-light3))
     (js2-function-param                        (:background nil :foreground icebox-bright_aqua))
     (js2-function-call                         (:background nil :foreground icebox-bright_blue))
     (js2-instance-member                       (:background nil :foreground icebox-bright_orange))
     (js2-private-member                        (:background nil :foreground icebox-faded_yellow))
     (js2-private-function-call                 (:background nil :foreground icebox-faded_aqua))
     (js2-jsdoc-html-tag-name                   (:background nil :foreground icebox-light4))
     (js2-jsdoc-html-tag-delimiter              (:background nil :foreground icebox-light3))

     ;; popup
     (popup-face                                (:underline nil :foreground icebox-light1 :background icebox-dark1))
     (popup-menu-mouse-face                     (:underline nil :foreground icebox-light0 :background icebox-faded_green))
     (popup-menu-selection-face                 (:underline nil :foreground icebox-light0 :background icebox-faded_green))
     (popup-tip-face                            (:underline nil :foreground icebox-light2 :background icebox-dark2))

     ;; helm
     (helm-M-x-key                              (:foreground icebox-bright_orange ))
     (helm-action                               (:foreground icebox-white :underline t))
     (helm-bookmark-addressbook                 (:foreground icebox-bright_red))
     (helm-bookmark-directory                   (:foreground icebox-bright_purple))
     (helm-bookmark-file                        (:foreground icebox-faded_blue))
     (helm-bookmark-gnus                        (:foreground icebox-faded_purple))
     (helm-bookmark-info                        (:foreground icebox-turquoise4))
     (helm-bookmark-man                         (:foreground icebox-sienna))
     (helm-bookmark-w3m                         (:foreground icebox-bright_yellow))
     (helm-buffer-directory                     (:foreground icebox-white :background icebox-bright_blue))
     (helm-buffer-not-saved                     (:foreground icebox-faded_red))
     (helm-buffer-process                       (:foreground icebox-burlywood4))
     (helm-buffer-saved-out                     (:foreground icebox-bright_red))
     (helm-buffer-size                          (:foreground icebox-bright_purple))
     (helm-candidate-number                     (:foreground icebox-bright_green))
     (helm-ff-directory                         (:foreground icebox-bright_purple))
     (helm-ff-executable                        (:foreground icebox-turquoise4))
     (helm-ff-file                              (:foreground icebox-sienna))
     (helm-ff-invalid-symlink                   (:foreground icebox-white :background icebox-bright_red))
     (helm-ff-prefix                            (:foreground icebox-black :background icebox-bright_yellow))
     (helm-ff-symlink                           (:foreground icebox-bright_orange))
     (helm-grep-cmd-line                        (:foreground icebox-bright_green))
     (helm-grep-file                            (:foreground icebox-faded_purple))
     (helm-grep-finish                          (:foreground icebox-turquoise4))
     (helm-grep-lineno                          (:foreground icebox-bright_orange))
     (helm-grep-match                           (:foreground icebox-bright_yellow))
     (helm-grep-running                         (:foreground icebox-bright_red))
     (helm-header                               (:foreground icebox-aquamarine4))
     (helm-helper                               (:foreground icebox-aquamarine4))
     (helm-history-deleted                      (:foreground icebox-black :background icebox-bright_red))
     (helm-history-remote                       (:foreground icebox-faded_red))
     (helm-lisp-completion-info                 (:foreground icebox-faded_orange))
     (helm-lisp-show-completion                 (:foreground icebox-bright_red))
     (helm-locate-finish                        (:foreground icebox-white :background icebox-aquamarine4))
     (helm-match                                (:foreground icebox-bright_orange))
     (helm-moccur-buffer                        (:foreground icebox-bright_aqua :underline t))
     (helm-prefarg                              (:foreground icebox-turquoise4))
     (helm-selection                            (:foreground icebox-white :background icebox-dark2))
     (helm-selection-line                       (:foreground icebox-white :background icebox-dark2))
     (helm-separator                            (:foreground icebox-faded_red))
     (helm-source-header                        (:foreground icebox-light2))
     (helm-visible-mark                         (:foreground icebox-black :background icebox-light3))

     ;; company-mode
     (company-scrollbar-bg                      (:background icebox-dark1))
     (company-scrollbar-fg                      (:background icebox-dark0_soft))
     (company-tooltip                           (:background icebox-dark0_soft))
     (company-tooltip-annotation                (:foreground icebox-bright_green))
     (company-tooltip-annotation-selection      (:inherit 'company-tooltip-annotation))
     (company-tooltip-selection                 (:foreground icebox-bright_purple :background icebox-dark2))
     (company-tooltip-common                    (:foreground icebox-bright_blue :underline t))
     (company-tooltip-common-selection          (:foreground icebox-bright_blue :underline t))
     (company-preview-common                    (:foreground icebox-light0))
     (company-preview                           (:background icebox-lightblue4))
     (company-preview-search                    (:background icebox-turquoise4))
     (company-template-field                    (:foreground icebox-black :background icebox-bright_yellow))
     (company-echo-common                       (:foreground icebox-faded_red))

     ;; tool tips
     (tooltip                                   (:foreground icebox-light1 :background icebox-dark1))

     ;; term
     (term-color-black                          (:foreground icebox-dark2 :background icebox-dark1))
     (term-color-blue                           (:foreground icebox-bright_blue :background icebox-bright_blue))
     (term-color-cyan                           (:foreground icebox-bright_aqua :background icebox-bright_aqua))
     (term-color-green                          (:foreground icebox-bright_green :background icebox-bright_green))
     (term-color-magenta                        (:foreground icebox-bright_purple :background icebox-bright_purple))
     (term-color-red                            (:foreground icebox-bright_red :background icebox-bright_red))
     (term-color-white                          (:foreground icebox-light1 :background icebox-light1))
     (term-color-yellow                         (:foreground icebox-bright_yellow :background icebox-bright_yellow))
     (term-default-fg-color                     (:foreground icebox-light0))
     (term-default-bg-color                     (:background icebox-bg))

     ;; message-mode
     (message-header-to                         (:inherit 'font-lock-variable-name-face))
     (message-header-cc                         (:inherit 'font-lock-variable-name-face))
     (message-header-subject                    (:foreground icebox-bright_orange :weight 'bold))
     (message-header-newsgroups                 (:foreground icebox-bright_yellow :weight 'bold))
     (message-header-other                      (:inherit 'font-lock-variable-name-face))
     (message-header-name                       (:inherit 'font-lock-keyword-face))
     (message-header-xheader                    (:foreground icebox-faded_blue))
     (message-separator                         (:inherit 'font-lock-comment-face))
     (message-cited-text                        (:inherit 'font-lock-comment-face))
     (message-mml                               (:foreground icebox-faded_green :weight 'bold))

     ;; org-mode
     (org-hide                                  (:foreground icebox-dark0))
     (org-level-1                               (:foreground icebox-bright_blue))
     (org-level-2                               (:foreground icebox-bright_yellow))
     (org-level-3                               (:foreground icebox-bright_purple))
     (org-level-4                               (:foreground icebox-bright_red))
     (org-level-5                               (:foreground icebox-bright_green))
     (org-level-6                               (:foreground icebox-bright_aqua))
     (org-level-7                               (:foreground icebox-faded_blue))
     (org-level-8                               (:foreground icebox-bright_orange))
     (org-special-keyword                       (:inherit 'font-lock-comment-face))
     (org-drawer                                (:inherit 'font-lock-function-face))
     (org-column                                (:background icebox-dark0))
     (org-column-title                          (:background icebox-dark0 :underline t :weight 'bold))
     (org-warning                               (:foreground icebox-bright_red :weight 'bold :underline nil :bold t))
     (org-archived                              (:foreground icebox-light0 :weight 'bold))
     (org-link                                  (:foreground icebox-faded_aqua :underline t))
     (org-footnote                              (:foreground icebox-bright_aqua :underline t))
     (org-ellipsis                              (:foreground icebox-light4))
     (org-date                                  (:foreground icebox-bright_blue :underline t))
     (org-sexp-date                             (:foreground icebox-faded_blue :underline t))
     (org-tag                                   (:bold t :weight 'bold))
     (org-list-dt                               (:bold t :weight 'bold))
     (org-todo                                  (:foreground icebox-bright_red :weight 'bold :bold t))
     (org-done                                  (:foreground icebox-bright_aqua :weight 'bold :bold t))
     (org-agenda-done                           (:foreground icebox-bright_aqua))
     (org-headline-done                         (:foreground icebox-bright_aqua))
     (org-table                                 (:foreground icebox-bright_blue))
     (org-formula                               (:foreground icebox-bright_yellow))
     (org-document-title                        (:foreground icebox-faded_blue))
     (org-document-info                         (:foreground icebox-faded_blue))
     (org-agenda-structure                      (:inherit 'font-lock-comment-face))
     (org-agenda-date-today                     (:foreground icebox-light0 :weight 'bold :italic t))
     (org-scheduled                             (:foreground icebox-bright_yellow))
     (org-scheduled-today                       (:foreground icebox-bright_blue))
     (org-scheduled-previously                  (:foreground icebox-faded_red))
     (org-upcoming-deadline                     (:inherit 'font-lock-keyword-face))
     (org-deadline-announce                     (:foreground icebox-faded_red))
     (org-time-grid                             (:foreground icebox-faded_orange))
     (org-latex-and-related                     (:foreground icebox-bright_blue))

     ;; org-habit
     (org-habit-clear-face                      (:background icebox-faded_blue))
     (org-habit-clear-future-face               (:background icebox-bright_blue))
     (org-habit-ready-face                      (:background icebox-faded_green))
     (org-habit-ready-future-face               (:background icebox-bright_green))
     (org-habit-alert-face                      (:background icebox-faded_yellow))
     (org-habit-alert-future-face               (:background icebox-bright_yellow))
     (org-habit-overdue-face                    (:background icebox-faded_red))
     (org-habit-overdue-future-face             (:background icebox-bright_red))

     ;; elfeed
     (elfeed-search-title-face                  (:foreground icebox-gray  ))
     (elfeed-search-unread-title-face           (:foreground icebox-light0))
     (elfeed-search-date-face                   (:inherit 'font-lock-builtin-face :underline t))
     (elfeed-search-feed-face                   (:inherit 'font-lock-variable-name-face))
     (elfeed-search-tag-face                    (:inherit 'font-lock-keyword-face))
     (elfeed-search-last-update-face            (:inherit 'font-lock-comment-face))
     (elfeed-search-unread-count-face           (:inherit 'font-lock-comment-face))
     (elfeed-search-filter-face                 (:inherit 'font-lock-string-face))

     ;; smart-mode-line
     (sml/global                                (:foreground icebox-burlywood4 :inverse-video nil))
     (sml/modes                                 (:foreground icebox-bright_green))
     (sml/filename                              (:foreground icebox-bright_red :weight 'bold))
     (sml/prefix                                (:foreground icebox-light1))
     (sml/read-only                             (:foreground icebox-bright_blue))
     (persp-selected-face                       (:foreground icebox-bright_orange))

     ;; powerline
     (powerline-active0                         (:background icebox-dark4 :foreground icebox-light0))
     (powerline-active1                         (:background icebox-dark3 :foreground icebox-light0))
     (powerline-active2                         (:background icebox-dark2 :foreground icebox-light0))
     (powerline-inactive0                       (:background icebox-dark2 :foreground icebox-light4))
     (powerline-inactive1                       (:background icebox-dark1 :foreground icebox-light4))
     (powerline-inactive2                       (:background icebox-dark0 :foreground icebox-light4))

     ;; isearch
     (isearch                                   (:foreground icebox-black :background icebox-bright_orange))
     (lazy-highlight                            (:foreground icebox-black :background icebox-bright_yellow))
     (isearch-fail                              (:foreground icebox-light0 :background icebox-bright_red))

     ;; markdown-mode
     (markdown-header-face-1                    (:foreground icebox-bright_blue))
     (markdown-header-face-2                    (:foreground icebox-bright_yellow))
     (markdown-header-face-3                    (:foreground icebox-bright_purple))
     (markdown-header-face-4                    (:foreground icebox-bright_red))
     (markdown-header-face-5                    (:foreground icebox-bright_green))
     (markdown-header-face-6                    (:foreground icebox-bright_aqua))

     ;; anzu-mode
     (anzu-mode-line                            (:foreground icebox-bright_yellow :weight 'bold))
     (anzu-match-1                              (:background icebox-bright_green))
     (anzu-match-2                              (:background icebox-faded_yellow))
     (anzu-match-3                              (:background icebox-aquamarine4))
     (anzu-replace-to                           (:foreground icebox-bright_yellow))
     (anzu-replace-highlight                    (:inherit 'isearch))

     ;; ace-jump-mode
     (ace-jump-face-background                  (:foreground icebox-light4 :background icebox-bg :inverse-video nil))
     (ace-jump-face-foreground                  (:foreground icebox-bright_red :background icebox-bg :inverse-video nil))

     ;; ace-window
     (aw-background-face                        (:forground  icebox-light1 :background icebox-bg :inverse-video nil))
     (aw-leading-char-face                      (:foreground icebox-bright_red :background icebox-bg :height 4.0))

     ;; show-paren
     (show-paren-match                          (:background icebox-dark3 :foreground icebox-bright_blue  :weight 'bold))
     (show-paren-mismatch                       (:background icebox-bright_red :foreground icebox-dark3 :weight 'bold))

     ;; ivy
     (ivy-current-match                         (:foreground icebox-light0_hard :weight 'bold :underline t))
     (ivy-minibuffer-match-face-1               (:foreground icebox-bright_orange))
     (ivy-minibuffer-match-face-2               (:foreground icebox-bright_yellow))
     (ivy-minibuffer-match-face-3               (:foreground icebox-faded_orange))
     (ivy-minibuffer-match-face-4               (:foreground icebox-faded_yellow))

     ;; ido
     (ido-only-match                            (:foreground icebox-faded_green))
     (ido-first-match                           (:foreground icebox-faded_green))
     (ido-subdir                                (:foreground icebox-faded_red))

     ;; magit
     (magit-bisect-bad                          (:foreground icebox-faded_red))
     (magit-bisect-good                         (:foreground icebox-faded_green))
     (magit-bisect-skip                         (:foreground icebox-faded_yellow))
     (magit-blame-heading                       (:foreground icebox-light0 :background icebox-dark2))
     (magit-branch-local                        (:foreground icebox-bright_blue))
     (magit-branch-current                      (:underline icebox-bright_blue :inherit 'magit-branch-local))
     (magit-branch-remote                       (:foreground icebox-bright_green))
     (magit-cherry-equivalent                   (:foreground icebox-bright_purple))
     (magit-cherry-unmatched                    (:foreground icebox-bright_aqua))
     (magit-diff-added                          (:foreground icebox-bright_green))
     (magit-diff-added-highlight                (:foreground icebox-bright_green :inherit 'magit-diff-context-highlight))
     (magit-diff-base                           (:background icebox-faded_yellow :foreground icebox-light2))
     (magit-diff-base-highlight                 (:background icebox-faded_yellow :foreground icebox-light0))
     (magit-diff-context                        (:foreground icebox-dark1  :foreground icebox-light1))
     (magit-diff-context-highlight              (:background icebox-dark1 :foreground icebox-light0))
     (magit-diff-hunk-heading                   (:background icebox-dark3 :foreground icebox-light2))
     (magit-diff-hunk-heading-highlight         (:background icebox-dark2 :foreground icebox-light0))
     (magit-diff-hunk-heading-selection         (:background icebox-dark2 :foreground icebox-bright_orange))
     (magit-diff-lines-heading                  (:background icebox-faded_orange :foreground icebox-light0))
     (magit-diff-removed                        (:foreground icebox-bright_red))
     (magit-diff-removed-highlight              (:foreground icebox-bright_red :inherit 'magit-diff-context-highlight))
     (magit-diffstat-added                      (:foreground icebox-faded_green))
     (magit-diffstat-removed                    (:foreground icebox-faded_red))
     (magit-dimmed                              (:foreground icebox-dark4))
     (magit-hash                                (:foreground icebox-bright_blue))
     (magit-log-author                          (:foreground icebox-bright_red))
     (magit-log-date                            (:foreground icebox-bright_aqua))
     (magit-log-graph                           (:foreground icebox-dark4))
     (magit-process-ng                          (:foreground icebox-bright_red :weight 'bold))
     (magit-process-ok                          (:foreground icebox-bright_green :weight 'bold))
     (magit-reflog-amend                        (:foreground icebox-bright_purple))
     (magit-reflog-checkout                     (:foreground icebox-bright_blue))
     (magit-reflog-cherry-pick                  (:foreground icebox-bright_green))
     (magit-reflog-commit                       (:foreground icebox-bright_green))
     (magit-reflog-merge                        (:foreground icebox-bright_green))
     (magit-reflog-other                        (:foreground icebox-bright_aqua))
     (magit-reflog-rebase                       (:foreground icebox-bright_purple))
     (magit-reflog-remote                       (:foreground icebox-bright_blue))
     (magit-reflog-reset                        (:foreground icebox-bright_red))
     (magit-refname                             (:foreground icebox-light4))
     (magit-section-heading                     (:foreground icebox-bright_yellow :weight 'bold))
     (magit-section-heading-selection           (:foreground icebox-faded_yellow))
     (magit-section-highlight                   (:background icebox-dark1))
     (magit-sequence-drop                       (:foreground icebox-faded_yellow))
     (magit-sequence-head                       (:foreground icebox-bright_aqua))
     (magit-sequence-part                       (:foreground icebox-bright_yellow))
     (magit-sequence-stop                       (:foreground icebox-bright_green))
     (magit-signature-bad                       (:foreground icebox-bright_red :weight 'bold))
     (magit-signature-error                     (:foreground icebox-bright_red))
     (magit-signature-expired                   (:foreground icebox-bright_orange))
     (magit-signature-good                      (:foreground icebox-bright_green))
     (magit-signature-revoked                   (:foreground icebox-bright_purple))
     (magit-signature-untrusted                 (:foreground icebox-bright_blue))
     (magit-tag                                 (:foreground icebox-bright_yellow))

     ;; git-gutter
     (git-gutter:modified                       (:background icebox-faded_blue :foreground icebox-faded_blue))
     (git-gutter:added                          (:background icebox-faded_green :foreground icebox-faded_green))
     (git-gutter:deleted                        (:background icebox-faded_red :foreground icebox-faded_red))

     ;; git-gutter+
     (git-gutter+-modified                      (:foreground icebox-faded_blue :background icebox-faded_blue))
     (git-gutter+-added                         (:foreground icebox-faded_green :background icebox-faded_green))
     (git-gutter+-deleted                       (:foreground icebox-faded_red :background icebox-faded_red))

     ;; git-gutter-fringe
     (git-gutter-fr:modified                    (:inherit 'git-gutter:modified))
     (git-gutter-fr:added                       (:inherit 'git-gutter:added))
     (git-gutter-fr:deleted                     (:inherit 'git-gutter:deleted))

     ;; flyspell
     (flyspell-duplicate                        (:underline (:color icebox-light4 :style 'line)))
     (flyspell-incorrect                        (:underline (:color icebox-bright_red :style 'line)))

     ;; langtool
     (langtool-errline                          (:foreground icebox-dark0 :background icebox-bright_red))
     (langtool-correction-face                  (:foreground icebox-bright_yellow :weight 'bold))

     ;; latex
     (font-latex-bold-face                      (:foreground icebox-faded_green :bold t))
     (font-latex-italic-face                    (:foreground icebox-bright_green :underline t))
     (font-latex-math-face                      (:foreground icebox-light3))
     (font-latex-script-char-face               (:foreground icebox-faded_aqua))
     (font-latex-sectioning-5-face              (:foreground icebox-bright_yellow :bold t))
     (font-latex-sedate-face                    (:foreground icebox-light4))
     (font-latex-string-face                    (:foreground icebox-bright_orange))
     (font-latex-verbatim-face                  (:foreground icebox-light4))
     (font-latex-warning-face                   (:foreground icebox-bright_red :weight 'bold))
     (preview-face                              (:background icebox-dark1))

     ;; mu4e
     (mu4e-header-key-face                      (:foreground icebox-bright_green :weight 'bold ))
     (mu4e-unread-face                          (:foreground icebox-bright_blue :weight 'bold ))
     (mu4e-highlight-face                       (:foreground icebox-bright_green))

     ;; shell script
     (sh-quoted-exec                            (:foreground icebox-bright_purple))
     (sh-heredoc                                (:foreground icebox-bright_orange))

     ;; undo-tree
     (undo-tree-visualizer-active-branch-face   (:foreground icebox-light0))
     (undo-tree-visualizer-current-face         (:foreground icebox-bright_red))
     (undo-tree-visualizer-default-face         (:foreground icebox-dark4))
     (undo-tree-visualizer-register-face        (:foreground icebox-bright_yellow))
     (undo-tree-visualizer-unmodified-face      (:foreground icebox-bright_aqua))

     ;; widget faces
     (widget-button-pressed-face                (:foreground icebox-bright_red))
     (widget-documentation-face                 (:foreground icebox-faded_green))
     (widget-field                              (:foreground icebox-light0 :background icebox-dark2))
     (widget-single-line-field                  (:foreground icebox-light0 :background icebox-dark2))

     ;; dired+
     (diredp-file-name                          (:foreground icebox-light2))
     (diredp-file-suffix                        (:foreground icebox-light4))
     (diredp-compressed-file-suffix             (:foreground icebox-faded_blue))
     (diredp-dir-name                           (:foreground icebox-faded_blue))
     (diredp-dir-heading                        (:foreground icebox-bright_blue))
     (diredp-symlink                            (:foreground icebox-bright_orange))
     (diredp-date-time                          (:foreground icebox-light3))
     (diredp-number                             (:foreground icebox-faded_blue))
     (diredp-no-priv                            (:foreground icebox-dark4))
     (diredp-other-priv                         (:foreground icebox-dark2))
     (diredp-rare-priv                          (:foreground icebox-dark4))
     (diredp-ignored-file-name                  (:foreground icebox-dark4))

     (diredp-dir-priv                           (:foreground icebox-faded_blue  :background icebox-dark_blue))
     (diredp-exec-priv                          (:foreground icebox-faded_blue  :background icebox-dark_blue))
     (diredp-link-priv                          (:foreground icebox-faded_aqua  :background icebox-dark_aqua))
     (diredp-read-priv                          (:foreground icebox-bright_red  :background icebox-dark_red))
     (diredp-write-priv                         (:foreground icebox-bright_aqua :background icebox-dark_aqua))

     ;; eshell
     (eshell-prompt-face                         (:foreground icebox-bright_aqua))
     (eshell-ls-archive-face                     (:foreground icebox-light3))
     (eshell-ls-backup-face                      (:foreground icebox-light4))
     (eshell-ls-clutter-face                     (:foreground icebox-bright_orange :weight 'bold))
     (eshell-ls-directory-face                   (:foreground icebox-bright_yellow))
     (eshell-ls-executable-face                  (:weight 'bold))
     (eshell-ls-missing-face                     (:foreground icebox-bright_red :bold t))
     (eshell-ls-product-face                     (:foreground icebox-faded_red))
     (eshell-ls-readonly-face                    (:foreground icebox-light2))
     (eshell-ls-special-face                     (:foreground icebox-bright_yellow :bold t))
     (eshell-ls-symlink-face                     (:foreground icebox-bright_red))
     (eshell-ls-unreadable-face                  (:foreground icebox-bright_red :bold t))

     ;; tabbar
     (tabbar-default                             (:foreground icebox-light0 :background icebox-dark3 :bold nil :height 1.0 :box (:line-width -5 :color icebox-dark3)))
     (tabbar-separator                           (:foreground icebox-light0 :background icebox-dark3))
     (tabbar-highlight                           (:inherit 'highlight))
     (tabbar-button                              (:foreground icebox-dark3 :background icebox-dark3 :box nil :line-width 0))
     (tabbar-button-highlight                    (:inherit 'tabbar-button :inverse-video t))
     (tabbar-modified                            (:foreground icebox-bright_green :background icebox-dark3 :box (:line-width -5 :color icebox-dark3)))
     (tabbar-unselected                          (:inherit 'tabbar-default))
     (tabbar-unselected-modified                 (:inherit 'tabbar-modified))
     (tabbar-selected                            (:inherit 'tabbar-default :foreground icebox-bright_yellow))
     (tabbar-selected-modified                   (:inherit 'tabbar-selected))

     ;; which-function-mode
     (which-func                                 (:foreground icebox-faded_blue)))
    ,@body))

(provide 'icebox)

;; Local Variables:
;; eval: (when (fboundp 'rainbow-mode) (rainbow-mode +1))
;; End:

;;; icebox-dark-theme.el ends here
